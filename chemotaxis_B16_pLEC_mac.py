# -*- coding: utf8 -*-

import numpy as np
import pandas as pd
from os import path

# Reading in data
data_dir = path.abspath(path.expanduser('data'))
df = pd.read_csv(path.join(data_dir,'chemotaxis_B16_pLEC_mac.csv'), index_col=False)
df = df.dropna(axis=0,how='any')
#df = df.groupby('Well').apply(lambda x: x.sum())
#df["ECT"].replace({'pLEC':1,'imLEC':2,'MS1':3}, inplace=True)
df = df.groupby('Well').mean()
#df["ECT"].replace({1:'pLEC',2:'imLEC',3:'MS1'}, inplace=True)


# Modelling
import statsmodels.api as sm
import statsmodels.formula.api as smf
formula = 'Value ~ C(Day) + pLECs*J774 + Hypoxia'
model = smf.ols(formula, df)
fit = model.fit()
summary = fit.summary()
with open("chemotaxis_B16_pLEC_macGLM.txt", "w") as text_file:
    text_file.write(summary.as_text())
print(sm.stats.anova_lm(fit, typ=1))
print(sm.stats.anova_lm(fit, typ=2))
print(sm.stats.anova_lm(fit, typ=3))
anova_summary = sm.stats.anova_lm(fit, typ=3)
with open("chemotaxis_B16_pLEC_macANOVA.txt", "w") as text_file:
    text_file.write(anova_summary.to_string())
